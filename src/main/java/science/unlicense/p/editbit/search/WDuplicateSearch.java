package science.unlicense.p.editbit.search;


import science.unlicense.api.Orderable;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.FillConstraint;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.api.layout.SplitConstraint;
import science.unlicense.api.path.Path;
import science.unlicense.engine.ui.component.path.WPathField;
import science.unlicense.engine.ui.ievent.ActionMessage;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.model.RowModel;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.engine.ui.widget.WTable;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.impl.cryptography.hash.CRC32;
import science.unlicense.impl.cryptography.hash.HashFunction;
import science.unlicense.impl.cryptography.hash.HashInputStream;

/**
 *
 * @author Johann Sorel
 */
public class WDuplicateSearch extends WContainer{

    private final WLabel searchPathLbl = new WLabel(new Chars("Path :"));
    private final WPathField searchPath = new WPathField();
    private final WButton search = new WButton(new Chars("Search and delete"));
    private final WTable list = new WTable();
    private final Sequence matches = new ArraySequence();

    public WDuplicateSearch(){
        super(new BorderLayout());
        final WSplitContainer split = new WSplitContainer();
        addChild(split, BorderConstraint.CENTER);

        final WContainer pane = new WContainer(new FormLayout());
        ((FormLayout)pane.getLayout()).setColumnSize(1, FormLayout.SIZE_EXPAND);
        pane.addChild(searchPathLbl,    FillConstraint.builder().coord(0,0).build());
        pane.addChild(searchPath,       FillConstraint.builder().coord(1,0).fill(true, false).build());
        pane.addChild(search,           FillConstraint.builder().coord(0,1).span(2,1).fill(true, false).build());

        final DefaultColumn col1 = new DefaultColumn(Chars.EMPTY, new ObjectPresenter() {
            @Override
            public Widget createWidget(Object candidate) {
                return new WLabel(((Path[])candidate)[0].getName());
            }
        });
        col1.setBestWidth(FormLayout.SIZE_EXPAND);

        final DefaultColumn col2 = new DefaultColumn(Chars.EMPTY, new ObjectPresenter() {
            @Override
            public Widget createWidget(Object candidate) {
                return new WLabel(((Path[])candidate)[1].getName());
            }
        });
        col2.setBestWidth(FormLayout.SIZE_EXPAND);
        list.getColumns().add(col1);
        list.getColumns().add(col2);

        split.addChild(pane,SplitConstraint.LEFT);
        split.addChild(list,SplitConstraint.RIGHT);

        RowModel model = new DefaultRowModel(matches);
        list.setRowModel(model);

        search.addEventListener(ActionMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                //list and create checksum for all files
                final Path path = searchPath.getPath();
                final Sequence files = new ArraySequence();
                try {
                    search(files,path);
                    Collections.sort(files);

                    //check all files with same checksum
                    final Sequence sameChecksums = new ArraySequence();
                    CheckPath p1, p2;
                    for(int i=0,n=files.getSize()-1;i<n;i++){
                        p1 = (CheckPath) files.get(i);
                        p2 = (CheckPath) files.get(i+1);
                        if(p1.checksum==p2.checksum){
                            if(sameChecksums.isEmpty()){
                                sameChecksums.add(p1);
                            }
                            sameChecksums.add(p2);
                        }else{
                            compare(sameChecksums);
                            sameChecksums.removeAll();
                        }
                    }
                    compare(sameChecksums);
                }catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    private void compare(Sequence files) throws IOException{
        for(int i=0;i<files.getSize();i++){
            final Path p1 = ((CheckPath)files.get(i)).path;
            for(int k=files.getSize()-1;k>i;k--){
                final Path p2 = ((CheckPath)files.get(k)).path;
                if(compare(p1, p2)){
                    matches.add(new Path[]{p1,p2});
                    files.remove(k);
                }
            }
        }
    }

    private static boolean compare(Path p1, Path p2) throws IOException{
        ByteInputStream in1 = p1.createInputStream();
        ByteInputStream in2 = p2.createInputStream();

        int i1,i2;
        for(;;){
            i1 = in1.read();
            i2 = in2.read();
            if(i1!=i2){
                //different files
                return false;
            }else if(i1==-1){
                break;
            }
        }
        in1.close();
        in2.close();

        
        System.out.println(p1.toURI()+" == "+p2.toURI());
        p2.delete();
        return true;
    }

    private static void search(Sequence files, Path path) throws IOException{
        if(path.isContainer()){
            final Iterator ite = path.getChildren().createIterator();
            while(ite.hasNext()) {
                search(files, (Path)ite.next());
            }
        }else{
            files.add(new CheckPath(path));
            if(files.getSize()%1000==0) System.out.println(files.getSize());
        }
    }

    private static final class CheckPath implements Orderable{
        private final int checksum;
        private final Path path;

        public CheckPath(Path path) throws IOException {
            this.path = path;
            ByteInputStream in = path.createInputStream();
            final HashFunction hf = new CRC32();
            in = new HashInputStream(hf, in);
            
            //limit to the first 20Ko
            int nb = 20000;
            for(long k=0;k>=0 && nb>0;k=in.skip(nb),nb-=k);
            in.close();
            checksum = (int) hf.getResultLong();
        }

        @Override
        public int order(Object other) {
            return checksum - ((CheckPath)other).checksum;
        }

    }

}