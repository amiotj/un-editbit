
package science.unlicense.p.editbit;

import science.unlicense.api.character.Chars;
import science.unlicense.api.desktop.UIFrame;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.FillConstraint;
import science.unlicense.api.layout.LineLayout;
import science.unlicense.api.model.tree.Node;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.io.RSReader;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.style.WStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.menu.WMenuButton;
import science.unlicense.engine.ui.widget.menu.WMenuDropDown;
import science.unlicense.impl.desktop.swing.SwingFrameManager;
import science.unlicense.system.path.Paths;
import science.unlicense.p.editbit.diff.WDiffPane;
import science.unlicense.p.editbit.search.WDuplicateSearch;
import science.unlicense.p.editbit.search.WFileSearchPane;
import science.unlicense.p.editbit.stream.WStreamView;

/**
 * Main frame.
 * 
 * @author Johann Sorel
 */
public class EditBitFrame {
    
    public static void main(String[] args) {

        final UIFrame frm = SwingFrameManager.INSTANCE.createFrame(false);
        frm.getContainer().setLayout(new BorderLayout());

        final WContainer pane = new WContainer(new BorderLayout());

        final WButtonBar bar = new WButtonBar();
        bar.setLayout(new LineLayout());
        final WMenuDropDown menu = new WMenuDropDown();
        menu.setText(new Chars("File"));

        final WMenuButton singleFile = new WMenuButton(new Chars("Bit view file"),null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                WStreamView v = new WStreamView();
                v.setLayoutConstraint(BorderConstraint.CENTER);
                pane.getChildren().replaceAll(new Node[]{v});
            }
        });

        final WMenuButton diffFiles = new WMenuButton(new Chars("Diff files"),null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                WDiffPane v = new WDiffPane();
                v.setLayoutConstraint(BorderConstraint.CENTER);
                pane.getChildren().replaceAll(new Node[]{v});
            }
        });

        final WMenuButton searchFiles = new WMenuButton(new Chars("Search files"),null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                WFileSearchPane v = new WFileSearchPane();
                v.setLayoutConstraint(BorderConstraint.CENTER);
                pane.getChildren().replaceAll(new Node[]{v});
            }
        });

        final WMenuButton searchDuplicatesFiles = new WMenuButton(new Chars("Search and remove duplicates"),null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                WDuplicateSearch v = new WDuplicateSearch();
                v.setLayoutConstraint(BorderConstraint.CENTER);
                pane.getChildren().replaceAll(new Node[]{v});
            }
        });
        
        menu.getDropdown().addChild(singleFile,null);
        menu.getDropdown().addChild(diffFiles,null);
        menu.getDropdown().addChild(searchFiles,null);
        menu.getDropdown().addChild(searchDuplicatesFiles,null);
        bar.addChild(menu,null);
        bar.addChild(createThemeButton(SystemStyle.THEME_LIGHT, new Chars(" Light ")), FillConstraint.builder().coord(1, 0).build());
        bar.addChild(createThemeButton(SystemStyle.THEME_DARK, new Chars(" Dark ")), FillConstraint.builder().coord(2, 0).build());

        frm.getContainer().addChild(bar, BorderConstraint.TOP);
        frm.getContainer().addChild(pane, BorderConstraint.CENTER);
        frm.setTitle(new Chars("Unlicense - EditBit"));
        frm.setSize(1024, 768);
        frm.setVisible(true);

    }

    private static WMenuButton createThemeButton(final Chars path, Chars name){
        final WMenuButton button = new WMenuButton(name,null,new EventListener(){
            @Override
            public void receiveEvent(Event event) {
                    WStyle style = RSReader.readStyle(Paths.resolve(path));
                    for(int i=0;i<style.getRules().getSize();i++){
                        final StyleDocument r = (StyleDocument) style.getRules().get(i);
                        WidgetStyles.mergeDoc(
                            SystemStyle.INSTANCE.getRule(r.getName()),
                            r, false);
                    }

                    SystemStyle.INSTANCE.notifyChanged();
            }
        });
        return button;
    }
    
}
