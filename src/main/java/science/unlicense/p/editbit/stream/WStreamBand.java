
package science.unlicense.p.editbit.stream;

import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.EventMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.layout.FillConstraint;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.api.layout.StackConstraint;
import science.unlicense.api.layout.StackLayout;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.api.painter2d.Font;
import science.unlicense.api.painter2d.FontMetadata;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.p.editbit.model.BitsEncoding;
import science.unlicense.p.editbit.model.DataEncoding;
import science.unlicense.p.editbit.model.PrimitiveEncoding;

/**
 *
 * @author Johann Sorel
 */
public class WStreamBand extends WContainer{

    private static final int BYTE_WIDTH = 30;

    private final WSpinner guiSpinner = new WSpinner(new NumberSpinnerModel(Integer.class, 0, 0, Integer.MAX_VALUE, 1));
    private BacktrackInputStream bs;
    private DataInputStream ds;
    private final DataEncoding enc;
    private WCells cells;

    public WStreamBand(final DataEncoding enc) {
        super(new BorderLayout());
        this.enc = enc;

        final WLabel lbl = new WLabel(enc.getName());
        lbl.getStyle().getSelfRule().setProperties(new Chars("background:{\nfill-paint:@color-background\n}"));
        lbl.setOverrideExtents(new Extents(80, 24));
        lbl.setHorizontalAlignment(WLabel.HALIGN_CENTER);
        guiSpinner.setOverrideExtents(new Extents(70, 24));
        guiSpinner.getStyle().getSelfRule().setProperties(new Chars("background:{fill-paint:@color-background\n}"));

        addChild(guiSpinner,BorderConstraint.LEFT);

        if(enc.getClass().equals(PrimitiveEncoding.class)) {
            cells = new WCells();
            addChild(cells,BorderConstraint.CENTER);
        } else {
            addChild(new WPaint(),BorderConstraint.CENTER);
        }
        addChild(lbl,BorderConstraint.RIGHT);

        guiSpinner.varValue().addListener(new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                setDirty();
            }
        });
    }

    public void setInput(BacktrackInputStream bs) {
        this.bs = bs;
        this.ds = new DataInputStream(bs);
        if(cells!=null) cells.update();
    }


    private class WCells extends WContainer {

        public WCells() {
            setOverrideExtents(new Extents(0, 0, 5, 5, 2000,2000));
            final FormLayout layout = new FormLayout();
            setLayout(layout);
        }

        @Override
        public void setEffectiveExtent(Extent extent) {
            super.setEffectiveExtent(extent);

            final int sizeInByte = enc.getBitSize() / 8 ;
            final int blockWidth = sizeInByte * BYTE_WIDTH;

            removeChildren();
            double width = extent.get(0);
            final FormLayout layout = (FormLayout) getLayout();

            int x = 0;
            while (width>0) {
                layout.setColumnSize(x, blockWidth);
                WPrimitiveCell cell = new WPrimitiveCell(x*sizeInByte);
                cell.setOverrideExtents(new Extents(0,0,5,Double.NaN,200,200));
                addChild(cell, new FillConstraint.Builder().coord(x, 0).fill().build());
                cell.update();
                width -= blockWidth;
                x++;
            }
        }

        public void update() {
            for (int x=0;x<getChildren().getSize();x++) {
                ((WPrimitiveCell)getChildNode(x)).update();
            }
        }

    }


    private class WPrimitiveCell extends WContainer {

        private final WLabel lbl = new WLabel();
        private WSpinner tf;
        private final int offset;

        private final EventListener up = new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    try {
                        byte[] data = enc.encode((Number)tf.varValue().getValue());
                        WStreamBand.this.getEventManager(true).sendEvent(new Event(WStreamBand.this, new WriteEvt(offset, data)));
                    }catch(Exception ex) {}
                }
            };

        public WPrimitiveCell(final int offset) {
            super(new StackLayout());
            this.offset = offset;
            lbl.setHorizontalAlignment(WLabel.HALIGN_CENTER);
            lbl.getStyle().getSelfRule().setProperties(new Chars("margin:2"));
            addChild(lbl, new StackConstraint(1));
            getStyle().getSelfRule().setProperties(new Chars("border:{\nbrush:plainbrush(1 'round')\nbrush-paint:@color-delimiter\n}"));

            WPrimitiveCell.this.varMouseOver().addListener(new EventListener() {
                public void receiveEvent(Event event) {
                    if (tf==null && isMouseOver()) {
                        tf = new WSpinner(new NumberSpinnerModel(enc.getValueClass(), 0, enc.getMinValue(), enc.getMaxValue(), 1));
                        addChild(tf, new StackConstraint(2));
                        tf.varVisible().sync(WPrimitiveCell.this.varMouseOver(),true);
                        update();
                    }
                }
            });
        }

        private void update() {
            if(bs==null) return;
            Chars text = Chars.EMPTY;
            bs.rewind();
            try {
                ds.skipFully(offset);
                Number v = enc.read(ds);
                text = new Chars("" + v);
                if (tf!=null) {
                    tf.varValue().removeListener(up);
                    tf.setValue(v);
                    tf.varValue().addListener(up);
                }
            }catch (IOException ex){
            }
            lbl.setText(text);
        }

    }

    private class WPaint extends WLeaf{

        public WPaint() {
            setView(new PaintView(WPaint.this));
        }

        private class PaintView extends WidgetView {

            public PaintView(Widget widget) {
                super(widget);
            }

            @Override
            protected void renderSelf(Painter2D painter, BBox dirtyArea, BBox innerBBox) {
                super.renderSelf(painter, dirtyArea, innerBBox);

                final Extent ext = getEffectiveExtent();
                final double height = ext.get(1);
                final double width = ext.get(0);
                final double minx = innerBBox.getMin(0);
                final double miny = innerBBox.getMin(1);

                int bx = ((Number)guiSpinner.getValue()).intValue();
                if(bx<0) bx = 0;
                int gx = bx*BYTE_WIDTH;
                final int blockWidth = enc.getBitSize()/8*BYTE_WIDTH;

                final ColorPaint textColor = new ColorPaint(SystemStyle.getSystemColor(SystemStyle.COLOR_TEXT));
                final ColorPaint mainColor = new ColorPaint(SystemStyle.getSystemColor(SystemStyle.COLOR_MAIN));
                final Color delimColor = SystemStyle.getSystemColor(SystemStyle.COLOR_DELIMITER);

                final Font font = new Font(new Chars[]{new Chars("Tuffy")}, 12, Font.WEIGHT_NONE);
                painter.setFont(font);
                painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
                painter.setPaint(new ColorPaint(delimColor));
                painter.stroke(new Segment(minx+0, miny+0, minx+width, miny+0));
                painter.setPaint(mainColor);

                if(ds==null) return;

                final int sizeInByte = (enc.getBitSize()/8);

                final FontMetadata metadata = font.getMetadata();
                double fontHOffset = (height - metadata.getGlyphBox().getSpan(1))/2;
                //float fontHOffset = (float) height;
                
                bs.rewind();

                while(gx<width){
                    painter.setPaint(mainColor);
                    painter.stroke(new Segment(minx+gx, miny+0, minx+gx, miny+height));
                    painter.setClip(new Rectangle(minx+gx, miny+0, enc.getBitSize()*BYTE_WIDTH, height));

                    if(enc instanceof BitsEncoding){
                        painter.stroke(new Segment(minx+gx, miny+height/2, minx+gx+blockWidth, miny+height/2));
                        painter.stroke(new Segment(minx+gx+(1*blockWidth)/4, miny+0, minx+gx+(1*blockWidth)/4, miny+height));
                        painter.stroke(new Segment(minx+gx+(2*blockWidth)/4, miny+0, minx+gx+(2*blockWidth)/4, miny+height));
                        painter.stroke(new Segment(minx+gx+(3*blockWidth)/4, miny+0, minx+gx+(3*blockWidth)/4, miny+height));
                        byte b;
                        try {
                            b = ds.readByte();
                        } catch (IOException ex) {
                            break;
                        }
                        if( (b & 0x80) !=0 ) painter.fill(new Rectangle(minx+gx+(0*blockWidth)/4, miny+0, blockWidth/4+1, height/2));
                        if( (b & 0x40) !=0 ) painter.fill(new Rectangle(minx+gx+(1*blockWidth)/4, miny+0, blockWidth/4+1, height/2));
                        if( (b & 0x20) !=0 ) painter.fill(new Rectangle(minx+gx+(2*blockWidth)/4, miny+0, blockWidth/4+1, height/2));
                        if( (b & 0x10) !=0 ) painter.fill(new Rectangle(minx+gx+(3*blockWidth)/4, miny+0, blockWidth/4+1, height/2));
                        if( (b & 0x08) !=0 ) painter.fill(new Rectangle(minx+gx+(0*blockWidth)/4, miny+height/2, blockWidth/4+1, height/2));
                        if( (b & 0x04) !=0 ) painter.fill(new Rectangle(minx+gx+(1*blockWidth)/4, miny+height/2, blockWidth/4+1, height/2));
                        if( (b & 0x02) !=0 ) painter.fill(new Rectangle(minx+gx+(2*blockWidth)/4, miny+height/2, blockWidth/4+1, height/2));
                        if( (b & 0x01) !=0 ) painter.fill(new Rectangle(minx+gx+(3*blockWidth)/4, miny+height/2, blockWidth/4+1, height/2));
                        painter.stroke(new Segment(minx+gx, miny+0, minx+gx+blockWidth, miny+0));
                        painter.stroke(new Segment(minx+gx, miny+height-1, minx+gx+blockWidth, miny+height-1));

                    }else{
                        Chars text = Chars.EMPTY;
                        try {
                            text = new Chars(""+enc.decode(ds));
                        } catch (IOException ex) {
                        }

                        final BBox textExtent = metadata.getCharsBox(text);

                        double cy = (height-textExtent.getSpan(1))/2.0
                                  + miny
                                  +metadata.getAscent();

                        painter.setPaint(textColor);
                        painter.fill(text, (float)minx+gx+2, (float) cy);
                    }

                    bx+= sizeInByte;
                    gx = bx*BYTE_WIDTH;
                }
            }
        }
    }

    public static class WriteEvt implements EventMessage {

        public int offset;
        public byte[] data;

        public WriteEvt(int offset, byte[] data) {
            this.offset = offset;
            this.data = data;
        }

        @Override
        public boolean isConsumable() {
            return false;
        }

        @Override
        public boolean isConsumed() {
            return false;
        }

        @Override
        public void consume() {
        }

    }

}
