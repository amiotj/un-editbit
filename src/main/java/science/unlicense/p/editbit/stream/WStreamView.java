
package science.unlicense.p.editbit.stream;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.api.desktop.UIFrame;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.MessagePredicate;
import science.unlicense.api.event.Property;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.SeekableByteBuffer;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.DisplacementLayout;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.layout.GridLayout;
import science.unlicense.api.layout.LineLayout;
import science.unlicense.api.math.Maths;
import science.unlicense.api.path.Path;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.component.path.WPathField;
import science.unlicense.engine.ui.ievent.ActionMessage;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WProgressBar;
import science.unlicense.engine.ui.widget.WScrollContainer;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.impl.desktop.swing.SwingFrameManager;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.p.editbit.model.BitsEncoding;
import science.unlicense.p.editbit.model.Constants;
import science.unlicense.p.editbit.model.DataEncoding;

/**
 *
 * @author Johann Sorel
 */
public class WStreamView extends WContainer {

    public static final Chars PROPERTY_PATH = new Chars("Path");

    private final WSpinner guiBitOffset = new WSpinner(new NumberSpinnerModel(Integer.class, 0, 0, Integer.MAX_VALUE, 1));
    private final WContainer guiGrid = new WContainer(new GridLayout(0, 1));
    private final WSpinner guiOffset = new WSpinner(new NumberSpinnerModel(Long.class, 0, 0, Long.MAX_VALUE, 1));
    private final WPathField guiPath = new WPathField();
    private final WLabel jLabel1 = new WLabel(new Chars(" Byte offset"));
    private final WLabel jLabel2 = new WLabel(new Chars(" Bit offset"));
    private final WContainer jPanel1 = new WContainer(new BorderLayout());
    private final WScrollContainer jScrollPane1 = new WScrollContainer(jPanel1);
    private final WButtonBar jToolBar1 = new WButtonBar();
    private final WProgressBar filePosition = new WProgressBar();

    //search
    private final WSpinner guiSearchValue = new WSpinner(new NumberSpinnerModel(Double.class, 0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 1));
    private final WButton guiSearch = new WButton(new Chars("Search"));

    private final Sequence bands = new ArraySequence();

    /**
     * Creates new form JStreamView
     */
    public WStreamView() {
        setLayout(new BorderLayout());
        jToolBar1.setLayout(new LineLayout());

        guiPath.varPath().addListener(new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                Path path = (Path) ((PropertyMessage)event.getMessage()).getNewValue();
                WStreamView.this.setPropertyValue(PROPERTY_PATH, path);
                updateData();
            }
        });

        guiOffset.setOverrideExtents(new Extents(120, 22));
        guiOffset.varValue().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                updateData();
            }
        });

        guiBitOffset.setOverrideExtents(new Extents(80, 22));
        guiBitOffset.varValue().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                updateData();
            }
        });

        guiSearchValue.setOverrideExtents(new Extents(120, 22));
        guiSearch.addEventListener(ActionMessage.PREDICATE,new EventListener() {
            public void receiveEvent(Event event) {
                search();
            }
        });
        jPanel1.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final MouseMessage message = (MouseMessage) event.getMessage();
                if (message.getType()==MouseMessage.TYPE_WHEEL) {
                    double r = -message.getWheelOffset();
                    long offset = ((Number)guiOffset.getValue()).longValue();
                    setByteOffset(offset+r);
                }
            }
        });

        jToolBar1.addChild(guiPath,null);
        jToolBar1.addChild(jLabel1,null);
        jToolBar1.addChild(guiOffset,null);
        jToolBar1.addChild(jLabel2,null);
        jToolBar1.addChild(guiBitOffset,null);
        jToolBar1.addChild(guiSearchValue,null);
        jToolBar1.addChild(guiSearch,null);

        addChild(jToolBar1, BorderConstraint.TOP);
        jPanel1.addChild(guiGrid, BorderConstraint.TOP);
        addChild(jScrollPane1, BorderConstraint.CENTER);
        addChild(filePosition, BorderConstraint.BOTTOM);
        final DisplacementLayout dispLayout = (DisplacementLayout) jScrollPane1.getScrollingContainer().getLayout();
        dispLayout.setFillWidth(true);


        for(DataEncoding de : Constants.DATA_ENC){
            final WStreamBand band = new WStreamBand(de);
            bands.add(band);
            band.addEventListener(new MessagePredicate(WStreamBand.WriteEvt.class), new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    System.out.println("event");
                    final Path path = getPath();
                    try {
                        final WStreamBand.WriteEvt we = (WStreamBand.WriteEvt) event.getMessage();
                        final SeekableByteBuffer sk = path.createSeekableBuffer(true, true, false);
                        sk.setPosition(((Number)guiOffset.getValue()).longValue()+we.offset);
                        sk.write(we.data);
                        sk.flush();
                        sk.dispose();

                    }catch(IOException ex) {
                        ex.printStackTrace();
                    }
                }
            });
        }

        guiGrid.getChildren().addAll(bands);

        updateData();

    }

    public void setByteOffset(Number offset) {
        guiOffset.setValue(offset.longValue());
    }

    public Path getPath() {
        return (Path) getPropertyValue(PROPERTY_PATH);
    }

    public void setPath(Path path) {
        setPropertyValue(PROPERTY_PATH, path);
    }

    public Property varPath() {
        return getProperty(PROPERTY_PATH);
    }

    private void updateData(){

        if(getPath()!= null){
            final long byteOffset = (Long)guiOffset.getValue();
            final int bitOffset = ((Number)guiBitOffset.getValue()).intValue();

            try {
                final Path path = getPath();
                Number size = (Number) path.getPathInfo(Path.INFO_OCTETSIZE);
                if (size!=null) {
                    filePosition.setProgress(Maths.clamp((double)byteOffset/size.longValue(),0.0,1.0) );
                    filePosition.setText(new Chars(""+byteOffset+" / "+size));
                }


                final DataInputStream ds = new DataInputStream(path.createInputStream());
                ds.skipFully(byteOffset);
                ds.readBits(bitOffset);

                BacktrackInputStream bs = new BacktrackInputStream(ds);
                bs.mark();

                for(int i=0;i<bands.getSize();i++){
                    ((WStreamBand)bands.get(i)).setInput(bs);
                }

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

    private void search(){
        final UIFrame frame = SwingFrameManager.INSTANCE.createFrame(false);
        frame.setTitle(new Chars("Search result"));
        final WContainer pane = frame.getContainer();
        pane.setLayout(new GridLayout(Constants.DATA_ENC.size(), 1));
        frame.setSize(800, 600);
        frame.setVisible(true);

        final double searched = ((Number)guiSearchValue.getValue()).doubleValue();

        for(int i=0;i<Constants.DATA_ENC.size();i++){
            final DataEncoding de = Constants.DATA_ENC.get(i);
            if(de instanceof BitsEncoding) continue;
            long maxVal = (1L << de.getBitSize()+1)-1;
            long minVal = -maxVal;
            if(de.getBitSize()<32 && (searched>maxVal || searched<minVal)) continue;

            new Thread(){
                public void run() {
                    System.out.println("Searching as : "+de.getName());

                    final CharBuffer sb = new CharBuffer();
                    try {
                        final BacktrackInputStream bs = new BacktrackInputStream(getPath().createInputStream());
                        final DataInputStream ds = new DataInputStream(bs);
                        final int step = 1024000;
                        long offset = 0;
                        for(long k=0;;k++){
                            bs.rewind();
                            bs.skip(k-offset);
                            if(k>0 && k%step==0){
                                //don't keep to much data backward
                                offset += step;
                                bs.mark();
                                System.out.println(k);
                            }

                            if(de.read(ds).doubleValue() == searched){
                                sb.append(Long.toString(k));
                                sb.append(',');
                            }
                        }

//                        final SeekableByteBuffer ss = file.createSeekableBuffer(true, false, false);
//                        final byte[] buffer = new byte[(de.getBitSize()+7)/8];
//                        for(long k=0,n=ss.getSize(); k<n; k++){
//                            ss.setPosition(k);
//                            ss.read(buffer);
//                            Number dd = (Number) de.decode(buffer, 0);
//                            if(dd.doubleValue() == searched){
//                                offset = ss.getPosition() - (de.getBitSize()/8);
//                                sb.append(offset);
//                                sb.append(',');
//                            }
//                        }


//                        final DataInputStream ds = new DataInputStream(file.createInputStream());
//                        for(;;){
//                            if(de.read(ds).doubleValue() == searched){
//                                offset = ds.getByteOffset() - (de.getBitSize()/8);
//                                sb.append(offset);
//                                sb.append(',');
//                            }
//                        }
                    } catch (Exception ex) {
                        // end of file or error, we don't care

                    }


                    final WContainer sub = new WContainer(new BorderLayout());
                    sub.addChild(new WLabel(de.getName()), BorderConstraint.LEFT);
                    final WLabel offsets = new WLabel(sb.toChars());
                    sub.addChild(offsets, BorderConstraint.CENTER);

                    pane.addChild(sub,null);
                }
            }.start();
        }


    }

}