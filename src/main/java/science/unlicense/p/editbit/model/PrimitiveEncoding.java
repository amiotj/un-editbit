
package science.unlicense.p.editbit.model;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;
import static science.unlicense.api.number.Primitive.*;

/**
 *
 * @author Johann Sorel
 */
public class PrimitiveEncoding implements DataEncoding {

    private final int type;
    private final NumberEncoding enc;

    public PrimitiveEncoding(int type, NumberEncoding enc) {
        this.type = type;
        this.enc = enc;
    }

    public NumberEncoding getNumberEncoding() {
        return enc;
    }

    public Chars getName() {
        Chars candidate = new Chars("");
        switch(type){
            case TYPE_1_BIT     : candidate = new Chars("1Bit"); break;
            case TYPE_2_BIT     : candidate = new Chars("2Bit"); break;
            case TYPE_4_BIT     : candidate = new Chars("4Bit"); break;
            case TYPE_BYTE      : candidate = new Chars("Byte"); break;
            case TYPE_UBYTE     : candidate = new Chars("UByte"); break;
            case TYPE_SHORT     : candidate = new Chars("Short"); break;
            case TYPE_USHORT    : candidate = new Chars("UShort"); break;
            case TYPE_INT24     : candidate = new Chars("Int24"); break;
            case TYPE_UINT24    : candidate = new Chars("UInt24"); break;
            case TYPE_INT       : candidate = new Chars("Int"); break;
            case TYPE_UINT      : candidate = new Chars("UInt"); break;
            case TYPE_LONG      : candidate = new Chars("Long"); break;
            case TYPE_ULONG     : candidate = new Chars("Ulong"); break;
            case TYPE_FLOAT     : candidate = new Chars("Float"); break;
            case TYPE_DOUBLE    : candidate = new Chars("Double"); break;
        }
        if(type>5){
            if(enc==NumberEncoding.BIG_ENDIAN){
                candidate = candidate.concat(new Chars(" BE"));
            }else{
                candidate = candidate.concat(new Chars(" LE"));
            }
        }
        return candidate;
    }

    public int getBitSize() {
        return Primitive.getSizeInBits(type);
    }

    public Object decode(byte[] buffer, int offset) throws IOException {
        Object candidate = new Chars("");
        switch(type){
//            case TYPE_1_BIT     : candidate = ds.readBits(1); break;
//            case TYPE_2_BIT     : candidate = ds.readBits(2); break;
//            case TYPE_4_BIT     : candidate = ds.readBits(4); break;
            case TYPE_BYTE      : candidate = enc.readByte(buffer, offset); break;
            case TYPE_UBYTE     : candidate = enc.readUByte(buffer, offset); break;
            case TYPE_SHORT     : candidate = enc.readShort(buffer, offset); break;
            case TYPE_USHORT    : candidate = enc.readUShort(buffer, offset); break;
            case TYPE_INT24     : candidate = enc.readInt24(buffer, offset); break;
            case TYPE_UINT24    : candidate = enc.readUInt24(buffer, offset); break;
            case TYPE_INT       : candidate = enc.readInt(buffer, offset); break;
            case TYPE_UINT      : candidate = enc.readUInt(buffer, offset); break;
            case TYPE_LONG      : candidate = enc.readLong(buffer, offset); break;
            case TYPE_ULONG     : candidate = enc.readLong(buffer, offset); break;
            case TYPE_FLOAT     : candidate = enc.readFloat(buffer, offset); break;
            case TYPE_DOUBLE    : candidate = enc.readDouble(buffer, offset); break;
        }
        return candidate;
    }

    public Object decode(DataInputStream ds) throws IOException {
        byte[] buffer = new byte[getBitSize()/8];
        ds.readFully(buffer);
        return decode(buffer, 0);
    }
    
    @Override
    public Number read(DataInputStream ds) throws IOException {
        Number candidate = 0;
        switch(type){
//            case TYPE_1_BIT     : candidate = ds.readBits(1); break;
//            case TYPE_2_BIT     : candidate = ds.readBits(2); break;
//            case TYPE_4_BIT     : candidate = ds.readBits(4); break;
            case TYPE_BYTE      : candidate = ds.readByte(); break;
            case TYPE_UBYTE     : candidate = ds.readUByte(); break;
            case TYPE_SHORT     : candidate = ds.readShort(enc); break;
            case TYPE_USHORT    : candidate = ds.readUShort(enc); break;
            case TYPE_INT24     : candidate = ds.readInt24(enc); break;
            case TYPE_UINT24    : candidate = ds.readUInt24(enc); break;
            case TYPE_INT       : candidate = ds.readInt(enc); break;
            case TYPE_UINT      : candidate = ds.readUInt(enc); break;
            case TYPE_LONG      : candidate = ds.readLong(enc); break;
            case TYPE_ULONG     : candidate = ds.readLong(enc); break;
            case TYPE_FLOAT     : candidate = ds.readFloat(enc); break;
            case TYPE_DOUBLE    : candidate = ds.readDouble(enc); break;
        }
        return candidate;
    }

    @Override
    public Class getValueClass() {
        switch(type){
            case TYPE_1_BIT     :
            case TYPE_2_BIT     :
            case TYPE_4_BIT     :
            case TYPE_BYTE      : return Byte.class;
            case TYPE_UBYTE     :
            case TYPE_SHORT     : return Short.class;
            case TYPE_USHORT    :
            case TYPE_INT24     :
            case TYPE_UINT24    :
            case TYPE_INT       : return Integer.class;
            case TYPE_UINT      :
            case TYPE_LONG      :
            case TYPE_ULONG     : return Long.class;
            case TYPE_FLOAT     : return Float.class;
            case TYPE_DOUBLE    : return Double.class;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public Number getMinValue() {
        switch(type){
            case TYPE_1_BIT     : return 0;
            case TYPE_2_BIT     : return 0;
            case TYPE_4_BIT     : return 0;
            case TYPE_BYTE      : return Byte.MIN_VALUE;
            case TYPE_UBYTE     : return 0;
            case TYPE_SHORT     : return Short.MIN_VALUE;
            case TYPE_USHORT    : return 0;
            case TYPE_INT24     : return -0x7FFFFF;
            case TYPE_UINT24    : return 0;
            case TYPE_INT       : return Integer.MIN_VALUE;
            case TYPE_UINT      : return 0;
            case TYPE_LONG      : return Long.MIN_VALUE;
            case TYPE_ULONG     : return Long.MIN_VALUE;
            case TYPE_FLOAT     : return -Float.MAX_VALUE;
            case TYPE_DOUBLE    : return -Double.MAX_VALUE;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public Number getMaxValue() {
        switch(type){
            case TYPE_1_BIT     : return 1;
            case TYPE_2_BIT     : return 3;
            case TYPE_4_BIT     : return 7;
            case TYPE_BYTE      : return Byte.MAX_VALUE;
            case TYPE_UBYTE     : return 0xFF;
            case TYPE_SHORT     : return Short.MAX_VALUE;
            case TYPE_USHORT    : return 0xFFFF;
            case TYPE_INT24     : return 0x7FFFFF;
            case TYPE_UINT24    : return 0xFFFFFF;
            case TYPE_INT       : return Integer.MAX_VALUE;
            case TYPE_UINT      : return 0xFFFFFFFFl;
            case TYPE_LONG      : return Long.MAX_VALUE;
            case TYPE_ULONG     : return Long.MAX_VALUE;
            case TYPE_FLOAT     : return Float.MAX_VALUE;
            case TYPE_DOUBLE    : return Double.MAX_VALUE;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public byte[] encode(Number value) throws IOException {
        byte[] data = new byte[8];
        switch(type){
//            case TYPE_1_BIT     : candidate = ds.readBits(1); break;
//            case TYPE_2_BIT     : candidate = ds.readBits(2); break;
//            case TYPE_4_BIT     : candidate = ds.readBits(4); break;
            case TYPE_BYTE      : enc.writeByte(value.byteValue(), data, 0); break;
            case TYPE_UBYTE     : enc.writeUByte(value.intValue(), data, 0); break;
            case TYPE_SHORT     : enc.writeShort(value.shortValue(), data, 0); break;
            case TYPE_USHORT    : enc.writeUShort(value.intValue(), data, 0); break;
            case TYPE_INT24     : enc.writeInt24(value.intValue(), data, 0); break;
            case TYPE_UINT24    : enc.writeUInt24(value.intValue(), data, 0); break;
            case TYPE_INT       : enc.writeInt(value.intValue(), data, 0); break;
            case TYPE_UINT      : enc.writeInt(value.intValue(), data, 0); break;
            case TYPE_LONG      : enc.writeLong(value.longValue(), data, 0); break;
            case TYPE_ULONG     : enc.writeLong(value.longValue(), data, 0); break;
            case TYPE_FLOAT     : enc.writeFloat(value.floatValue(), data, 0); break;
            case TYPE_DOUBLE    : enc.writeDouble(value.doubleValue(), data, 0); break;
        }
        return Arrays.copy(data, 0, getBitSize()/8);
    }

}