
package science.unlicense.p.editbit.model;

import java.util.ArrayList;
import java.util.List;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.path.Path;
import science.unlicense.impl.math.diff.DiffEngine;

/**
 *
 * @author Johann Sorel
 */
public class Constants {
    
    
    public static final List<DataEncoding> DATA_ENC = new ArrayList<DataEncoding>();
    static {
        DATA_ENC.add(new BitsEncoding());
        DATA_ENC.add(new HexEncoding());
        DATA_ENC.add(new CharEncoding());
        
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_BYTE, NumberEncoding.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_UBYTE, NumberEncoding.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_SHORT, NumberEncoding.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_USHORT, NumberEncoding.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_INT24, NumberEncoding.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_UINT24, NumberEncoding.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_INT, NumberEncoding.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_UINT, NumberEncoding.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_LONG, NumberEncoding.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_ULONG, NumberEncoding.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_FLOAT, NumberEncoding.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_DOUBLE, NumberEncoding.BIG_ENDIAN));
        
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_SHORT, NumberEncoding.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_USHORT, NumberEncoding.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_INT24, NumberEncoding.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_UINT24, NumberEncoding.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_INT, NumberEncoding.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_UINT, NumberEncoding.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_LONG, NumberEncoding.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_ULONG, NumberEncoding.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_FLOAT, NumberEncoding.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.TYPE_DOUBLE, NumberEncoding.LITTLE_ENDIAN));
    }
    
    public static Sequence makeDiff(Path file1, Path file2) throws IOException{
        ByteInputStream is = file1.createInputStream();
        final byte[] file1Bytes = IOUtilities.readAll(is);
        is.close();
        is = file2.createInputStream();
        final byte[] file2Bytes = IOUtilities.readAll(is);
        is.close();
        
        final DiffEngine engine = new DiffEngine();
        return engine.processDiff(new ByteSequence(file1Bytes), new ByteSequence(file2Bytes));
    }
    
}
