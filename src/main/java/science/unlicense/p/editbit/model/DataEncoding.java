
package science.unlicense.p.editbit.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public interface DataEncoding {

    Chars getName();

    Class getValueClass();

    Number getMinValue();

    Number getMaxValue();

    int getBitSize();

    Object decode(byte[] buffer, int offset) throws IOException;

    byte[] encode(Number value) throws IOException;

    Object decode(DataInputStream ds) throws IOException;
    
    Number read(DataInputStream ds) throws IOException;

}