
package science.unlicense.p.editbit.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;

/**
 *
 * @author Johann Sorel
 */
public class BitsEncoding extends PrimitiveEncoding{

    public BitsEncoding() {
        super(Primitive.TYPE_UBYTE, NumberEncoding.BIG_ENDIAN);
    }

    @Override
    public Chars getName() {
        return new Chars("Bits");
    }
    
}
