
package science.unlicense.p.editbit.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;

/**
 *
 * @author Johann Sorel
 */
public class HexEncoding extends PrimitiveEncoding{

    public HexEncoding() {
        super(Primitive.TYPE_UBYTE, NumberEncoding.BIG_ENDIAN);
    }

    @Override
    public Chars getName() {
        return new Chars("Hexa");
    }

    @Override
    public Object decode(byte[] buffer, int offset) throws IOException {
        Number n = (Number) super.decode(buffer,offset);
        return Integer.toHexString(n.intValue()).toUpperCase();
    }
    
}
