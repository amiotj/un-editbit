
package science.unlicense.p.editbit.diff;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.event.PropertyPredicate;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.io.IOException;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.GridLayout;
import science.unlicense.api.layout.LineLayout;
import science.unlicense.api.layout.SplitConstraint;
import science.unlicense.api.path.Path;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.p.editbit.model.Constants;
import science.unlicense.p.editbit.stream.WStreamView;

/**
 *
 * @author Johann Sorel
 */
public class WDiffPane extends WContainer{

    private final WStreamView view1 = new WStreamView();
    private final WStreamView view2 = new WStreamView();
    private final WDiffView diff1 = new WDiffView(true);
    private final WDiffView diff2 = new WDiffView(false);
    
    public WDiffPane() {
        super(new BorderLayout());
        
        final WSplitContainer split = new WSplitContainer(true);
        final WContainer infoPane = new WContainer(new LineLayout());
        final WContainer diffsPane = new WContainer(new BorderLayout());
        final WContainer gridTop = new WContainer(new GridLayout(1,2,4,4));
        final WContainer gridBottom = new WContainer(new GridLayout(1,2,4,4));
        infoPane.addChild(new WLabel(new Chars("No change"),new WGraphicImage(createIcon(WDiffView.DIFF_NOCHANGE))),null);
        infoPane.addChild(new WLabel(new Chars("Added"),new WGraphicImage(createIcon(WDiffView.DIFF_ADD))),null);
        infoPane.addChild(new WLabel(new Chars("Removed"),new WGraphicImage(createIcon(WDiffView.DIFF_REMOVE))),null);
        infoPane.addChild(new WLabel(new Chars("Replaced"),new WGraphicImage(createIcon(WDiffView.DIFF_REPLACE))),null);
        gridTop.addChild(view1,null);
        gridTop.addChild(view2,null);
        gridBottom.addChild(diff1,null);
        gridBottom.addChild(diff2,null);
        diffsPane.addChild(infoPane, BorderConstraint.TOP);
        diffsPane.addChild(gridBottom, BorderConstraint.CENTER);
        
        split.addChild(gridTop,SplitConstraint.LEFT);
        split.addChild(diffsPane,SplitConstraint.RIGHT);
        
        addChild(split, BorderConstraint.CENTER);
        
        final EventListener lst = new EventListener() {
            public void receiveEvent(Event event) {
                updateDiff();
            }
        };        
        view1.varPath().addListener(lst);
        view2.varPath().addListener(lst);

        diff1.addEventListener(new PropertyPredicate(WDiffView.PROPERTY_OFFSET), new EventListener() {
            public void receiveEvent(Event event) {
                final int offset = (Integer)((PropertyMessage)event.getMessage()).getNewValue();
                view1.setByteOffset(offset);
            }
        });
        diff2.addEventListener(new PropertyPredicate(WDiffView.PROPERTY_OFFSET), new EventListener() {
            public void receiveEvent(Event event) {
                final int offset = (Integer)((PropertyMessage)event.getMessage()).getNewValue();
                view2.setByteOffset(offset);
            }
        });
        
    }
    
    private void updateDiff(){
        final Path path1 = view1.getPath();
        final Path path2 = view2.getPath();
        if(path1!=null && path2!=null){
            try {
                Sequence diff = Constants.makeDiff(path1, path2);
                diff1.setDiffs(diff);
                diff2.setDiffs(diff);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
        }
        
    }
    
    private static Image createIcon(ColorPaint color){
        final Image img = Images.create(new Extent.Long(16, 16), Images.IMAGE_TYPE_RGBA);
        img.getColorModel().asTupleBuffer(img).setARGB(new BBox(img.getExtent()), color.getColor().toARGB());
        return img;
    }
    
}
